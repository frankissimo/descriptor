import csv
import os
import time
from os import makedirs
from os.path import join
from pathlib import Path

import cv2
from PIL import Image
from pandas import np
from sift import drawMatchesKnn_and_save_image, drawMatches_and_save_image_ransac, ransac
from utils import crop_image_GRAY_SCALE


def drawMatchesKnn_and_save(matchesMask, matches, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path):
    draw_params = dict(matchColor=(0, 255, 0),
                       singlePointColor=(255, 0, 0),
                       matchesMask=matchesMask,
                       flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

    img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, matches, None, **draw_params)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    cv2.imwrite(join(save_path, image_name_2.split('/')[len(image_name_2.split('/')) - 1]), img3)





def drawMatches_and_save_ransac(matchesMask, good, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path):
    draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                       singlePointColor=None,
                       matchesMask=matchesMask,  # draw only inliers
                       flags=2)

    img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)
    cv2.imwrite(join(save_path, image_name_2.split('/')[len(image_name_2.split('/')) - 1]) + "ransac.png", img3)



def surf_function_test(image_name_1, image_name_2,path,filename):
    """
    im1 = Image.open(image_name_1)
    IMAGE_10 = os.path.join('tmp1.jpeg')
    im1.save(IMAGE_10, "JPEG", quality=10)

    img1 = cv2.imread(IMAGE_10,
                      cv2.IMREAD_GRAYSCALE)  # queryImage

    im1 = Image.open(image_name_2)
    IMAGE_20 = os.path.join('tmp2.jpeg')
    im1.save(IMAGE_20, "JPEG", quality=10)

    img2 = cv2.imread(IMAGE_20,
                      cv2.IMREAD_GRAYSCALE)  # trainImage

    img1 = cv2.cvtColor(cv2.imread(IMAGE_10), cv2.COLOR_BGR2RGB)

    img2 = cv2.cvtColor(cv2.imread(IMAGE_20), cv2.COLOR_BGR2RGB)
    """
    img1 = cv2.imread(image_name_1,
                      cv2.IMREAD_GRAYSCALE)  # queryImage
    img2 = cv2.imread(image_name_2,
                      cv2.IMREAD_GRAYSCALE)  # trainImage

    #img1 = crop_image_GRAY_SCALE(image_name_1, component.bounds)

    # Initiate  detector

    print("..based_matcher SURF processing...")

    print(image_name_1)
    print(image_name_2)
    ptr = cv2.xfeatures2d.SURF_create()
    # if mode.lower() == "surf":
    # ptr = cv2.xfeatures2d.SURF_create()
    # print("..based_matcher SURF processing...")
    # find the keypoints and descriptors with SIFT/SURF

    kp1, des1 = ptr.detectAndCompute(img1, None)
    kp2, des2 = ptr.detectAndCompute(img2, None)
    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=10)  # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)
    # Need to draw only good matches, so create a mask
    simplemask = [[0, 0] for i in range(len(matches))]
    good = []
    # ratio test as per Lowe's paper
    for i, (m, n) in enumerate(matches):
        if m.distance <0.8 * n.distance:
            simplemask[i] = [1, 0]
            good.append(m)

    # plt.imshow(img3, ), plt.show()
    #save_path = join('matches', image_name_1.split('/')[0], image_name_1.split('/')[1],image_name_1.split('/')[2], image_name_2.split("/")[0])

    #save_path = join(os.path.dirname(os.path.realpath(__file__)), 'surf', save_path)
    # save image without ransac method
    #drawMatchesKnn_and_save(matchesMask, matches, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path)

    ## ransac method applied
    ransac_matches = ransac(good, 20, img1, img2, kp1, kp2)
    #drawMatches_and_save_ransac(ransac_matches, good, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path)
    # clusters = Meanshift(good, ransac_matches, kp1, save_path, image_name_2, start, img1)
    # calculate meanshift clusters after ransac

    drawMatches_and_save_image_ransac(ransac_matches, good, img1, kp1, img2, kp2, path, filename + "ransac.png")

    new_good = [good[x] for x in range(len(good))]
    # LEMDS_matches = LEMDS(new_good, 20, img1, img2, kp1, kp2)

    # drawMatches_and_save_image_ransac(LEMDS_matches, new_good, img1, kp1, img2, kp2, path, filename + "ledms.png")

    drawMatchesKnn_and_save_image(simplemask, matches, img1, kp1, img2, kp2, path, filename)

    return simplemask, ransac_matches, good, kp1, kp2