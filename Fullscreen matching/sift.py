import os
from os import makedirs
from os.path import join
from pathlib import Path
import pygcransac

import cv2

from PIL import Image
from pandas import np

from utils import crop_image, crop_image_GRAY_SCALE


def drawMatchesKnn_and_save(matchesMask, matches, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path):
    draw_params = dict(matchColor=(0, 255, 0),
                       singlePointColor=(255, 0, 0),
                       matchesMask=matchesMask,
                       flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

    img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, matches, None, **draw_params)
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    cv2.imwrite(join(save_path, image_name_2.split('/')[len(image_name_2.split('/')) - 1]), img3)


def ransac_vecchio(good_matches, MIN_MATCH_COUNT, img1, img2, kp1, kp2):
    matchesMask = None
    if len(good_matches) >= MIN_MATCH_COUNT:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 500, maxIters=50)

        if M is not None:
            matchesMask = mask.ravel().tolist()
            h, w = img1.shape
            pts = np.float32([[0, 0], [0, - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)
            # img2 = cv2.polylines(img2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)

    else:
        print
        "Not enough matches are found - %d/%d" % (len(good_matches), MIN_MATCH_COUNT)

    return matchesMask
def ransac(good_matches, MIN_MATCH_COUNT, img1, img2, kp1, kp2):
    matchesMask = None
    if len(good_matches) >= MIN_MATCH_COUNT:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 2)
        h1 = img1.shape[0]
        w1 = img1.shape[1]
        h2 = img2.shape[0]
        w2 = img2.shape[1]

        M, mask = pygcransac.findHomography(src_pts, dst_pts, h1, w1, h2, w2, 100.0)
        # M, mask = cv2.findHomography(src_pts, dst_pts, cv2.FM_RANSAC, 250, maxIters=2000)

        if M is not None:
            matchesMask = mask.ravel().tolist()
            h = img1.shape[0]
            w = img1.shape[1]
            pts = np.float32([[0, 0], [0, - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)
            # img2 = cv2.polylines(img2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)


    else:
        print
        "Not enough matches are found - %d/%d" % (len(good_matches), MIN_MATCH_COUNT)

    return matchesMask


def LEMDS(good_matches, MIN_MATCH_COUNT, img1, img2, kp1, kp2):
    matchesMask = None
    if len(good_matches) >= MIN_MATCH_COUNT:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.LMEDS, 35, maxIters=1000)

        if M is not None:
            matchesMask = mask.ravel().tolist()
            h, w = img1.shape
            pts = np.float32([[0, 0], [0, - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
            dst = cv2.perspectiveTransform(pts, M)
            # img2 = cv2.polylines(img2, [np.int32(dst)], True, 255, 3, cv2.LINE_AA)

    else:
        print
        "Not enough matches are found - %d/%d" % (len(good_matches), MIN_MATCH_COUNT)

    return matchesMask


def drawMatches_and_save_ransac(matchesMask, good, img1, kp1, image_name_1, image_name_2, img2, kp2, save_path):
    draw_params = dict(matchColor=(0, 255, 0),  # draw matches in green color
                       singlePointColor=None,
                       matchesMask=matchesMask,  # draw only inliers
                       flags=2)

    img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)
    cv2.imwrite(join(save_path, image_name_2.split('/')[len(image_name_2.split('/')) - 1]) + "ransac.png", img3)





def sift_function_test(image_name_1, image_name_2, path, filename):
    """
    im1 = Image.open(image_name_1)
    IMAGE_10 = os.path.join('tmp1.jpeg')
    im1.save(IMAGE_10, "JPEG", quality=10)


    img1 = cv2.imread(IMAGE_10,
                      cv2.IMREAD_GRAYSCALE)  # queryImage

    im1 = Image.open(image_name_2)
    IMAGE_20 = os.path.join('tmp2.jpeg')
    im1.save(IMAGE_20, "JPEG", quality=10)

    img2 = cv2.imread(IMAGE_20,
                      cv2.IMREAD_GRAYSCALE)  # trainImage

    img1 = cv2.cvtColor(cv2.imread(IMAGE_10), cv2.COLOR_BGR2RGB)

    img2 = cv2.cvtColor(cv2.imread(IMAGE_20), cv2.COLOR_BGR2RGB)

    """
    img1 = cv2.imread(image_name_1,
                      cv2.IMREAD_GRAYSCALE)  # queryImage
    img2 = cv2.imread(image_name_2,
                      cv2.IMREAD_GRAYSCALE)

    #img1 = crop_image_GRAY_SCALE(image_name_1, component.bounds)



    # trainImage


    # Initiate  detector
    print("..based_matcher SIFT processing...")
    ptr = cv2.xfeatures2d.SIFT_create()
    kp1, des1 = ptr.detectAndCompute(img1, None)
    kp2, des2 = ptr.detectAndCompute(img2, None)
    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=10)  # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des1, des2, k=2)
    # Need to draw only good matches, so create a mask
    simplemask = [[0, 0] for i in range(len(matches))]
    good = []
    # ratio test as per Lowe's paper
    for i, (m, n) in enumerate(matches):
        if m.distance < 0.9* n.distance:
            simplemask[i] = [1, 0]
            good.append(m)

    ## ransac method applied
    ransac_matches = ransac(good, 10, img1, img2, kp1, kp2)

    # simplemask  contain a valeu of [1,0] for all element of matches with correct threshold
    # the same value are contained in good

    drawMatches_and_save_image_ransac(ransac_matches, good, img1, kp1, img2, kp2, path, filename+"ransac.png")

    new_good = [good[x] for x in range(len(good)) ]
    #LEMDS_matches = LEMDS(new_good, 20, img1, img2, kp1, kp2)

    #drawMatches_and_save_image_ransac(LEMDS_matches, new_good, img1, kp1, img2, kp2, path, filename + "ledms.png")



    drawMatchesKnn_and_save_image(simplemask, matches, img1, kp1, img2, kp2, path, filename)

    return simplemask, ransac_matches, good, kp1, kp2


def drawMatchesKnn_and_save_image(matchesMask, matches, img1, kp1, img2, kp2, path, file_name):
    draw_params = dict(matchColor=(0, 255, 0),
                       singlePointColor=(255, 0, 0),
                       matchesMask=matchesMask,
                       flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

    img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, matches, None, **draw_params)
    if not os.path.exists(path):
        os.makedirs(path)
    cv2.imwrite(join(path, file_name), img3)


def drawMatches_and_save_image_ransac(matchesMask, matches, img1, kp1, img2, kp2, path, file_name):
    draw_params = dict(matchColor=(0, 255, 0),
                       singlePointColor=(255, 0, 0),
                       matchesMask=matchesMask,
                       flags=2)

    img3 = cv2.drawMatches(img1, kp1, img2, kp2, matches, None, **draw_params)
    if not os.path.exists(path):
        os.makedirs(path)
    cv2.imwrite(join(path, file_name), img3)
