from pandas import np


def get_points_of_component(src_pts,dst_pts,main_component):
    correct_pts=[]
    dst_correct=0
    for point_src in range(len(src_pts)):
        if main_component.bounds.x_min < src_pts[point_src][0] < main_component.bounds.x_max:
            if main_component.bounds.y_min < src_pts[point_src][1] < main_component.bounds.y_max:
                correct_pts.append([int(dst_pts[point_src][0]), int(dst_pts[point_src][1])])
                dst_correct += 1

    if dst_correct==len(correct_pts):
        return correct_pts
    else:
        return None


def index_component(point_of_max,list_of_objects):
    list_of_components_matched=[]
    for i in list_of_objects:
        if i.bounds.x_min < point_of_max[0] < i.bounds.x_max:
            if i.bounds.y_min < point_of_max[1] < i.bounds.y_max:
                list_of_components_matched.append(i)

    return list_of_components_matched


def check_match(component,list_of_components_matched,check_max_point):

    for i in list_of_components_matched:
        if check_max_point==True :
            if (i.element_path==component.element_path or (i.resource_id==component.resource_id and (i.resource_id=="" or i.resource_id==None)
                    and (i.text==component.text and (i.text==""or i.text==None) )
                    and (i.content_desc==component.content_desc and (i.content_desc=="" or i.content_desc==None)))):
                return i, True


            if (i.resource_id == component.resource_id and ((i.resource_id != "" and i.resource_id != None) and( (i.text == component.text and (i.text != "" and i.text != None))
                    or (i.content_desc == component.content_desc and (
                    i.content_desc != "" and i.content_desc != None))))):

                return i,True


    return None,False


