import csv
import os
from os.path import join
from collections import Counter

from PIL import Image

from akaze import akaze_function_test
from sift import sift_function_test
from surf import surf_function_test
from utils import matching_components, save_elements
from sklearn.cluster import MeanShift
from sklearn.metrics import precision_score, recall_score, confusion_matrix

import numpy as np
import datetime

from Csv_writer import write_row
from testing_utils import get_points_of_component, index_component, check_match
from utils import crop_image, composed_cropped,composed_cropped_2
from xml_read import bounds

source_path = join(os.path.curdir, "devices_test")
constant_dimension = 100
first_line_total = [0, 0, 0]


def testing_screenshot(methods_main, methods_other):
    descriptor_list = ["sift", "surf", "akaze"]
    first_line = [1, 1, 1]

    for descriptor_index in range(len(descriptor_list)):
        correct_interaction = [0 for x in descriptor_list]

        image_test_main = methods_main.path_image

        image_test_dev = methods_other.path_image

        path = join(os.path.curdir, source_path, "results", descriptor_list[descriptor_index],
                    methods_main.device + "_" + methods_other.device, methods_main.app)
        path_csv = join(os.path.curdir, source_path, "results", descriptor_list[descriptor_index],
                        methods_main.device + "_" + methods_other.device
                        )

        if not os.path.exists(path):
            os.makedirs(path)
        filename = "screenshot.jpg"
        # set time of starting process
        first_time = datetime.datetime.now()

        simplemask, ransac_matches, good, kp1, kp2 = None, None, None, None, None



        if descriptor_index == 0:
            simplemask, ransac_matches, good, kp1, kp2 = sift_function_test(image_test_main, image_test_dev, path,
                                                                            filename)
        if descriptor_index == 1:
            simplemask, ransac_matches, good, kp1, kp2 = surf_function_test(image_test_main,
                                                                            image_test_dev, path, filename)
        if descriptor_index == 2:
            simplemask, ransac_matches, good, kp1, kp2 = akaze_function_test(image_test_main,
                                                                             image_test_dev, path, filename)


        if ransac_matches != None:
            # if ransac ransac found matches

            src_pts = [kp1[good[x].queryIdx].pt for x in range(len(good)) if ransac_matches[x] == 1]
            dst_pts = [kp2[good[x].trainIdx].pt for x in range(len(good)) if ransac_matches[x] == 1]
        else:
            # if ransac ransac not found matches
            src_pts = [kp1[good[x].queryIdx].pt for x in range(len(good))]
            dst_pts = [kp2[good[x].trainIdx].pt for x in range(len(good))]

        descriptor_time = datetime.datetime.now()
        """
        set y_true and y_preds for performance measurement
        
        """


        y_preds = [ None for x in range(len(methods_main.ui_objects))]
        y_true = [None for x in range(len(methods_main.ui_objects))]

        for component in methods_main.ui_objects:
            #set time to identify cluster time process
            component_time_start = datetime.datetime.now()

            check_max_point = True
            """
            check if component is present in both screenshot
            
            
            """

            item,item_found=check_match(component, methods_other.ui_objects, check_max_point)
            if item_found==True:
                y_true[methods_main.ui_objects.index(component)] = 1
            else:
                y_true[methods_main.ui_objects.index(component)] = 0


            """ 
            get_points_of_component retrieve dst keypoint matched 
            
            """
            correct_pts = get_points_of_component(src_pts, dst_pts, component)
            point_of_max = None
            check_max_point = False
            if len(correct_pts) != 0:
                # retrieve cluster with max number of keypoints
                clustering = MeanShift(bandwidth=0.1).fit(np.array(correct_pts))
                c = Counter(clustering.labels_)
                cluster_max = c.most_common(1)[0][0]
                point_of_max = clustering.cluster_centers_[cluster_max]
                check_max_point = True






            else:
                """
                there are not keypoints dst matched !!!! 
                
                """
                y_preds[methods_main.ui_objects.index(component)] = 0
                check_max_point = False

            #cropped_image1 contain cropped image of widget
            cropped_image1 = crop_image(image_test_main, component.bounds)


            item_found=None
            if check_max_point == True:
                #save component matching

                cropped_image2 = crop_image(image_test_dev, bounds([point_of_max[0] - (constant_dimension / 2),
                                                                    point_of_max[1] - (constant_dimension / 2),
                                                                    point_of_max[0] + (constant_dimension / 2),
                                                                    point_of_max[1] + (constant_dimension / 2)]))
                new_im = composed_cropped_2(cropped_image1, cropped_image2)
                list_of_components_matched = index_component(point_of_max, methods_other.ui_objects)
                component_matched,item_found = check_match(component, list_of_components_matched, check_max_point)
            else:
                new_im = composed_cropped(cropped_image1)

            if not os.path.exists(join(path, "singolar_matching")):
                os.makedirs(join(path, "singolar_matching"))
            new_im.save(join(path, "singolar_matching", methods_main.ui_objects.index(component).__str__() + ".jpg"))

            if first_line[descriptor_index] == 1:
                # filename, mode='a', row=[]
                write_row(join(path_csv, methods_main.app, "banchmark_detailed.csv"), 'a', ["device_main",
                                                                                            "other device",
                                                                                            "app",

                                                                                            "action_value_main",
                                                                                            "action_value_other",
                                                                                            "resource_id_main",
                                                                                            "resource_id_other",
                                                                                            "text_main", "text_other",
                                                                                            "content_desc_main",
                                                                                            "content_desc_other",
                                                                                            "android_class_main",
                                                                                            "android_class_other",
                                                                                            "x_min_main", "y_min_main",
                                                                                            "x_max_main", "y_max_main",
                                                                                            "x_min_other",
                                                                                            "y_min_other",
                                                                                            "x_max_other",
                                                                                            "y_max_other",
                                                                                            "width_screen_main",
                                                                                            "height_screen_main",
                                                                                            "width_screen_other",
                                                                                            "height_screen_other",
                                                                                            "time_process"

                                                                                            ])

                first_line[descriptor_index] += 1

            later_time = datetime.datetime.now()
            time_descriptor=  descriptor_time-first_time
            time_processing = later_time - component_time_start

            if item_found==True :
                y_preds[methods_main.ui_objects.index(component)] = 1
                write_row(join(path_csv, methods_main.app, "banchmark_detailed.csv"), 'a',
                          [methods_main.device,
                           methods_other.device,
                           methods_main.app.replace(",","").replace(";",","),
                           component.action_value.__str__(), component.action_value.__str__(),
                           component.resource_id, component_matched.resource_id,
                           component.text.__str__(), component_matched.text.__str__(),
                           component.content_desc.__str__(), component_matched.content_desc.__str__(),
                           component.android_class.__str__(), component_matched.android_class.__str__(),
                           component.bounds.get_string(), component_matched.bounds.get_string(),
                           methods_main.get_dimension_string(), methods_other.get_dimension_string(),
                           time_descriptor.total_seconds().__str__(),
                           time_processing.total_seconds().__str__(),

                           item_found])
                correct_interaction[descriptor_index] += 1
            else:
                y_preds[methods_main.ui_objects.index(component)] = 0
                write_row(join(path_csv, methods_main.app, "banchmark_detailed.csv"), 'a',
                          [methods_main.device,
                           methods_other.device,
                           methods_main.app.replace(",","").replace(";",","),
                           component.action_value.__str__(), "None",
                           component.resource_id.__str__(), "None",
                           component.text.__str__(), "None",
                           component.content_desc.__str__(), "None",
                           component.android_class.__str__(), "None",
                           component.bounds.get_string(), "None", "None", "None", "None",
                           methods_main.get_dimension_string(), methods_other.get_dimension_string(),
                           time_descriptor.total_seconds().__str__(),
                           time_processing.total_seconds().__str__(),

                           False])

        precision = precision_score(y_true, y_preds, average='binary')
        recall = recall_score(y_true, y_preds, average='binary')


        if first_line_total[descriptor_index] == 0:
            write_row(join(path_csv, "banchmark.csv"), 'a',
                      ["descriptor", "main_device", "other_device",
                       "App",
                       "Screenshot", "precision", "recall",
                       " correct matching"])
            first_line_total[descriptor_index] += 1

        write_row(join(path_csv, "banchmark.csv"), 'a',
                  [descriptor_list[descriptor_index], methods_main.device, methods_other.device,
                   methods_main.app.replace(",","").replace(";",","), "Screenshot_0", precision.__str__(), recall.__str__()])


        a=[descriptor_list[descriptor_index], methods_main.device, methods_other.device,
                   methods_main.app.replace(",", "").replace(";", ","), "Screenshot_0"]
        a.extend(y_true)
        write_row(join(path_csv, "banchmark_metrics.csv"), 'a',a)
        a= ["", "", "","", "" ]
        a.extend(y_preds)
        write_row(join(path_csv, "banchmark_metrics.csv"), 'a', a)


def testing_app(main_app, other_app):
    testing_screenshot(main_app, other_app)
