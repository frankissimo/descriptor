import csv
import os
from os.path import join

from PIL import Image

from App_testing import testing_app
from xml_read import read_xml


def rindex(mylist, myvalue):
    return len(mylist) - mylist[::-1].index(myvalue) - 1

 

def main_test():
    """
    parser = argparse.ArgumentParser(description="Match keypoint");
    parser.add_argument('--main_model', type=str, help='input keypoint file', required=True)
    parser.add_argument('--mode', type=str, help='mode permitted are: SIFT, SURF AKAZE and ALL', required=True)
    parser.add_argument('--application_name', type=str, help='Insert an application name to run only it')
    args = parser.parse_args()
    args.main_model
    """
    #

    for main_model in ["Nexus_S_API","Google Nexus 5","Pixel_3_XL_API_25"]:

        data_devices = "data"
        #list of all devices
        list_of_devices = [f for f in os.listdir(join(os.path.curdir,data_devices)) if os.path.isdir(join(os.path.curdir,data_devices,f))]

        #app list
        all_devices_apps= [set(os.listdir(join(data_devices,f1)))  for f1 in list_of_devices]


        #app choosen for testing
        app_for_testing = set.intersection(*all_devices_apps)






        #in 100 main - vs -main is not considered
        list_of_devices.remove(main_model)


        # read app for main device
        test_main = read_xml(main_model, data_devices,app_for_testing)


        for dev in list_of_devices:
            # read app for target device
            other_app = read_xml(dev, data_devices,app_for_testing)
            for app in range(len(test_main.app_array)):
                print(test_main.app_array[app].xml_path)
                print(other_app.app_array[app].xml_path)
                print(test_main.device_name)
                print(other_app.device_name)

                testing_app(test_main.app_array[app],[ tmp for tmp in other_app.app_array if tmp.app==test_main.app_array[app].app][0])







if __name__ == "__main__":
    main_test()
