import csv
import datetime
import os
from datetime import time
from datetime import datetime
from datetime import timedelta
from os.path import join


from xml_read import read_xml
from collections import Counter
from Csv_writer import write_row
from testing_utils import get_points_of_component, index_component, check_match
import math

descriptors=["sift","surf","akaze"]



devices=["Google Nexus 5_","Google_Nexus_4_","Nexus_S_API_","Pixel_3_XL_API_25_"]





def average(x):
    ''' average(x) x is a vector '''
    d=len(x)
    av=0
    for e in x:
        av = av + e
    av = float(av) / d
    return av

def sig2(x,av=None):
    ''' sig2(x,av=None) x is a vector '''
    d=len(x)
    if not av :
        av = average(x)
    s2=0
    for e in x:
        dev = e - av
        s2 = s2 + dev*dev
    if d > 1 :
        s2 = float(s2) / (d - 1)
    return s2


def get_unique_components():
    android_component = []

    for dev in ["Pixel_2_XL_API_25", "Pixel_3_XL_API_25", "Nexus_S_API", "Google_Nexus_4", "Google Nexus 5"]:

        data_devices = "data"
        main_model = dev
        list_of_devices = [f for f in os.listdir(join(os.path.curdir, data_devices)) if
                           os.path.isdir(join(os.path.curdir, data_devices, f))]
        # list of apps for all devices

        all_devices_apps = [set(os.listdir(join(data_devices, f1))) for f1 in list_of_devices]

        # list of apps contained in the device with minor apps
        # app_for_testing= [ f1 for f1 in all_devices_apps if len(f1)==min([len(f) for f in all_devices_apps])][0]

        app_for_testing = set.intersection(*all_devices_apps)

        to_remove = ["3Q_Photo_Diary_4.6.4", "Daily_Quran_1.0",
                     "FlashLight_11.0.3",
                     "Joplin_1.0.281",
                     "OneCloud_Text_Editor_2.0",
                     "Out_of_Milk_8.11.0_904",
                     "PassMax_1.0",
                     "WordPad_1.0"]

        for i in to_remove:
            app_for_testing.remove(i)

        list_of_devices.remove(main_model)
        # app_for_testing=["Open_Note_1.2.2016"]

        # list_of_devices=["Pixel_3_XL_API_25"]

        test_main = read_xml(main_model, data_devices, app_for_testing)

        for dev in list_of_devices:

            other_app = read_xml(dev, data_devices, app_for_testing)
            different_total_device = 0

            for app in range(len(test_main.app_array)):
                different_items = 0

                print(test_main.app_array[app].xml_path)
                print(other_app.app_array[app].xml_path)
                print(test_main.device_name)
                print(other_app.device_name)

                methods_main = test_main.app_array[app]
                methods_other = [tmp for tmp in other_app.app_array if tmp.app == test_main.app_array[app].app][0]

                for component in methods_main.ui_objects:

                    check_max_point = True
                    if dev == "Google Nexus 5":
                        android_component.append(component.android_class)

                    item, item_found = check_match(component, methods_other.ui_objects, check_max_point)

                    if item_found != True:
                        different_items += 1

                write_row("different_component_detailed.csv", 'a',
                          [methods_main.device, methods_other.device, methods_main.app, different_items])
                different_total_device += different_items

            write_row("different_component.csv", 'a',
                      [methods_main.device, methods_other.device, different_total_device])

    class_component = Counter(android_component)

    for key, value in class_component.items():
        write_row("class_component.csv", 'a', [key.__str__(), value.__str__()])



def calculate_average_recall_precision():
    path = 'devices_test'
    write_row("metrics_overview.csv", "a", ["device", "descriptors", "precision", "recall","std_precision","std_recall"])
    for device in devices:
        metrics = [[[], [], 0], [[], [], 0], [[], [], 0]]

        files = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(join(os.path.curdir,path)):
            for file in f:
                if 'banchmark.csv' in file and device in r:
                    files.append(os.path.join(r, file))
        for f in files:
            print(f.__str__())
            precision_score=0
            recall_score=0
            with open(f) as csvfile:
                readCSV = csv.reader(csvfile, delimiter=';')
                next(readCSV, None)  # skip the headers
                for i in readCSV:
                    index_descriptor=descriptors.index(f.split(os.sep)[2])

                    precision_score=float(i[5])
                    recall_score=float(i[6])
                    metrics[index_descriptor][0].append(precision_score)
                    metrics[index_descriptor][1].append(recall_score)
                    metrics[index_descriptor][2] += 1
        if metrics!= [[[], [], 0], [[], [], 0], [[], [], 0]]:
            for i in range(len(metrics)):
                average_precision=sum(metrics[i][0])/metrics[i][2]
                average_recall = sum(metrics[i][1]) / metrics[i][2]
                std_precision=sig2(metrics[i][0])
                std_recall = sig2(metrics[i][1])
                write_row("metrics_overview.csv","a",[device,descriptors[i],average_precision.__str__(),average_recall.__str__(),std_precision.__str__(),std_recall.__str__()])


def calculate_times_metrics():
    path = 'devices_test'
    #write_row("metrics_overview.csv", "a", ["device", "descriptors", "precision", "recall"])
    for device in devices:
        time_metrics = [[[],[]], [[], []], [[],[]]]

        files = []
        # r=root, d=directories, f = files
        for r, d, f in os.walk(join(os.path.curdir, path)):
            for file in f:
                if 'banchmark_detailed.csv' in file and device in r:
                    print(os.path.join(r, file))
                    files.append(os.path.join(r, file))
        for f in files:
            with open(f.__str__(), encoding="latin-1") as csvfile:
                row=0
                readCSV = csv.reader(csvfile, delimiter=';')
                total_app = datetime.strptime("0.0", '%S.%f')
                next(readCSV, None)  # skip the headers
                total_app = datetime.strptime("0.0", '%S.%f')
                for i in readCSV:
                    if row==0:
                        row+=1
                        if len(i) > 20:
                            total_app= datetime(1900, 1, 1) + timedelta(seconds=float(i[20]))
                        else:
                            total_app = datetime(1900, 1, 1) + timedelta(seconds=float(i[17]))





                    index_descriptor = descriptors.index(f.split(os.sep)[2])
                    print(i.__str__())

                    if len(i) > 20:
                        time_score= datetime(1900, 1, 1) + timedelta(seconds=float(i[20]))
                        time_score= time_score+ timedelta(seconds=float(i[21]))

                        total_app = total_app + timedelta(seconds=float(i[21]))

                    else:
                        time_score = datetime(1900, 1, 1) + timedelta(seconds=float(i[17]))
                        time_score = time_score + timedelta(seconds=float(i[18]))

                        total_app = total_app + timedelta(seconds=float(i[18]))

                    time_metrics[index_descriptor][0].append(time_score)


                time_metrics[index_descriptor][1].append(total_app)

        for i in range(len(time_metrics)):
            total_times_elements = datetime.strptime("0.0", '%S.%f')
            total_times_app = datetime.strptime("0.0", '%S.%f')

            for f in time_metrics[i][0]:
                total_times_elements = total_times_elements + timedelta(seconds=f.second, microseconds=f.microsecond)
                if time_metrics[i][0].index(f) != 0:
                    average = (total_times_elements - datetime.strptime("0.0", '%S.%f')).total_seconds() / \
                              time_metrics[i][0].index(f)

            for f in time_metrics[i][1]:
                total_times_app = total_times_app + timedelta(seconds=f.second) + timedelta(
                    microseconds=f.microsecond)



            average = (total_times_elements - datetime.strptime("0.0", '%S.%f')).total_seconds() / len(time_metrics[i][0])
            average_app = (total_times_app - datetime.strptime("0.0", '%S.%f')).total_seconds() / len(time_metrics[i][1])

            write_row("metrics_overview_time.csv", "a",
                      [device, descriptors[i], (average_app).__str__(), (average).__str__()])


if __name__ == "__main__":

    #get_unique_components()
    calculate_average_recall_precision()
    calculate_times_metrics()



