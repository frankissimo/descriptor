import csv
import os
from os.path import join

import cv2
from PIL import Image


def save_elements(matching_relationship, methods_main_steps, image_test_main, path, type):
    for m_b in range(len(methods_main_steps.ui_objects)):
        box1_main = [methods_main_steps.ui_objects[m_b].bounds.x_min,
                     methods_main_steps.ui_objects[m_b].bounds.y_min,
                     methods_main_steps.ui_objects[m_b].bounds.x_max,
                     methods_main_steps.ui_objects[m_b].bounds.y_max]
        image_main = Image.open(image_test_main)
        cropped_image1 = image_main.crop(box1_main)
        images = [cropped_image1]
        widths, heights = zip(*(i.size for i in images))
        total_width = sum(widths)
        max_height = max(heights)
        new_im = Image.new('RGB', (total_width, max_height))
        x_offset = 0
        for im in images:
            new_im.paste(im, (x_offset, 0))
            x_offset += im.size[0]

        if not os.path.exists(join(path, type)):
            os.makedirs(join(path, type))

        new_im.save(join(path, type, m_b.__str__() + ".jpg"))


def matching_components(methods_main_steps, methods_other_steps, src_pts, dst_pts):
    matching_relationship = [0 for i in range(len(methods_main_steps.ui_objects))]
    for m_b in methods_main_steps.ui_objects:
        vector_match_components = [0 for i in range(len(methods_other_steps.ui_objects))]
        component_main_bounds = m_b.bounds
        src_points = 0
        for point_src in range(len(src_pts)):
            if component_main_bounds.x_min <= src_pts[point_src][0] <= component_main_bounds.x_max:
                if component_main_bounds.y_min <= src_pts[point_src][1] <= component_main_bounds.y_max:
                    src_points += 1
                    for t_b in methods_other_steps.ui_objects:
                        component_other_bounds = t_b.bounds
                        if component_other_bounds.x_min <= dst_pts[point_src][
                            0] <= component_other_bounds.x_max:
                            if component_other_bounds.y_min <= dst_pts[point_src][
                                1] <= component_other_bounds.y_max:
                                vector_match_components[methods_other_steps.ui_objects.index(t_b)] += 1

        index_of_matched_element = vector_match_components.index(max(vector_match_components))
        matching_relationship[methods_main_steps.ui_objects.index(m_b)] = index_of_matched_element

    return matching_relationship


def crop_image(image, bounds):
    box1_main = [bounds.x_min,

                 bounds.y_min,
                 bounds.x_max,
                 bounds.y_max]

    image_main = Image.open(image)

    cropped_image1 = image_main.crop(box1_main)
    return cropped_image1


def crop_image_GRAY_SCALE(image, bounds):
    box1_main = [bounds.x_min,

                 bounds.y_min,
                 bounds.x_max,
                 bounds.y_max]

    img1 = cv2.imread(image,
                      cv2.IMREAD_GRAYSCALE)

    cropped_image1 = img1[int(bounds.y_min):int(bounds.y_max), int(bounds.x_min):int(bounds.x_max)]




    return cropped_image1


def composed_cropped_2(cropped_image1, cropped_image2):
    images = [cropped_image1, cropped_image2]
    widths, heights = zip(*(i.size for i in images))
    total_width = sum(widths)
    max_height = max(heights)
    new_im = Image.new('RGB', (total_width, max_height))
    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset, 0))
        x_offset += im.size[0]

    return new_im

def composed_cropped(cropped_image1):
    images = [cropped_image1]
    widths, heights = zip(*(i.size for i in images))
    total_width = sum(widths)
    max_height = max(heights)
    new_im = Image.new('RGB', (total_width, max_height))
    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset, 0))
        x_offset += im.size[0]

    return new_im



